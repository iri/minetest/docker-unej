FROM nytimes/blender:2.92-cpu-ubuntu18.04

ARG BUILD_DATE
ARG VERSION

LABEL build_version="iri-research.org version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="I.R.I."

ENV INSTANCE_MOD_PATH "/usr/local/share/minetest/games/unej/mods"
ENV LOCAL_MOD_PATH "/usr/local/share/minetest/games/unej/mods"

RUN apt-get update && apt-get install -y \
    imagemagick \
    && rm -rf /var/lib/apt/lists/*

COPY game /usr/local/share/minetest/games/unej
COPY meshport2gltf/meshport2gltf.sh /usr/local/sbin/meshport2gltf.sh
COPY meshport2gltf/blender /usr/local/lib/blender
